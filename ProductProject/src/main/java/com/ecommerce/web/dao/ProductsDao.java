package com.ecommerce.web.dao;
import com.ecommerce.products.Products;
import com.ecommerce.web.*;
import java.io.*;
import com.ecommerce.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class ProductsDao {
	public List<Products> getProducts()
	{
		List<Products> users=new ArrayList<>();
			Products p = null;
			try {
				Class.forName("org.mariadb.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce","root","Gopal@1This");
				
				Statement stmt = con.createStatement();
				String s = ("SELECT * from ecommerce");
				ResultSet rs = stmt.executeQuery(s);
				
				while(rs.next()) {
					String category =rs.getString("category");
					String Pname =rs.getString("Product_name");
					int Pid = rs.getInt("Product_id");
					int Pprice=rs.getInt("Product_price");
					int Pquantity=rs.getInt("Product_quantity");
					String Pdesc=rs.getString("Product_desc");
					String Image_url=rs.getString("product_image");
					p = new Products(category,Pname, Pid, Pprice, Pquantity, Pdesc,Image_url);
					users.add(p);
				}
				rs.close();
				stmt.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e);
			}
			return users;
    }
		public int addInventory(Products prods) throws ClassNotFoundException {
			int record=0;
			try {
				Class.forName("org.mariadb.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce", "root", "Gopal@1This");
				System.out.println(prods);
				PreparedStatement ps = con.prepareStatement("insert into ecommerce values(?,?,?,?,?,?,?)");
				ps.setString(1, prods.getCategory());
				ps.setString(3, prods.getPname());
				ps.setInt(2, prods.getPid());
				ps.setInt(5, prods.getPprice());
				ps.setInt(6, prods.getPquantity());
	            ps.setString(4, prods.getPdesc());
	            ps.setString(7, prods.getImage_url());
	            record=ps.executeUpdate();
	            ps.close();
	            return record;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return record;
	}
		public int deleteProductUsingId(int pid) throws ClassNotFoundException {
			int delete=0;
			try {
				Class.forName("org.mariadb.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce", "root", "Gopal@1This");
				PreparedStatement ps = con.prepareStatement("delete from ecommerce where Product_id=?");
				ps.setInt(1, pid);
				
				delete=ps.executeUpdate();
				System.out.println("Row deleted sucessfully :"+delete);
				ps.close();
				return delete;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return delete;
	}
	public int updateProductUsingId(String category, String pname, int pid, int pprice, int pquantity, String pdesc,String image_url )
		{
			int rs2 = 0;
			try {
				Class.forName("org.mariadb.jdbc.Driver");
			    Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/ecommerce", "root", "Gopal@1This");
			    PreparedStatement ps2 = con.prepareStatement("UPDATE ecommerce SET category=?,Product_name=?,Product_price = ?,Product_desc=?,Product_quantity=?,product_image=? WHERE Product_id=?");
			    ps2.setString(1,category);
			    ps2.setString(2,pname);
			    ps2.setInt(3, pprice);
			    ps2.setString(4, pdesc);
			    ps2.setInt(5,pquantity);
			    ps2.setInt(7,pid);
			    ps2.setString(6, image_url);
			    rs2=ps2.executeUpdate();
			    System.out.println("The  product price has updated sucessfully"+rs2);
			    ps2.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return rs2;
	}
}